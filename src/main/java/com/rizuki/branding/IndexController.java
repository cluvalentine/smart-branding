package com.rizuki.branding;

import com.rizuki.egov.webutil.endpoint.EndpointRequestBuilder;
import com.rizuki.egov.webutil.json.JsonResponseReader;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.HttpMethod;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jetty.http.HttpHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Muhammad Rizki
 */
@Named
@ViewScoped
public class IndexController implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private final static Logger LOG = LoggerFactory.getLogger(IndexController.class);
	
	@Inject
	private JatisEndpoint jatisEndpoint;
	@Inject
	private FacesContext facesContext;
	@Inject
	private ConfigRepository configRepository;
	
	private String phoneNumber;
	private String smsContent;
	
	@PostConstruct
	public void init() {
		smsContent = "Selamat datang di kota Bandung, ibu kota provinsi Jawa Barat.\n"
				+ "Silakan menikmati keindahan kota ini disertai berbagai kuliner khas kota Bandung.\n"
				+ "1. Batagor;\n"
				+ "2. Bakso Bandung;\n"
				+ "3.Seblak.\n "
				+ "Informasi lebih lanjut kunjungi http://duniakulinerbandung.com/home/";
	}
	
	public void sendSMS() {
		try {
			if (StringUtils.isNotEmpty(phoneNumber)) {
				if (phoneNumber.contains(",")) {
					String[] number = phoneNumber.split(",");
					for (String receiver : number) {
						receiver = receiver.trim();
						sendToJatis(receiver);
					}
				} else {
					sendToJatis(phoneNumber);
				}
				
				facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Info terkirim"));
			} else {
				facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed", "Silakan isi nomor tujuan"));
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed", "Layanan tidak tersedia"));
		}
		
	}
	
	private void sendToJatis(String smsReceiver) {
		String baseUrl = configRepository.getConfigValue(Config.JATIS_URL);
		baseUrl = baseUrl.replace("@phone", smsReceiver);
		
		String message = smsContent;
		message = message.replaceAll(" ", "%20");
		message = message.replaceAll("\n", "%0A");
		
		baseUrl = baseUrl.replace("@message", message);
		baseUrl = baseUrl.replaceAll("@channel", SmsJatisChannelEnum.ALERT_SMS.getValue());
		
		jatisEndpoint.initialize(baseUrl);
		LOG.info("Base URL : " + baseUrl);
		try {
			String response = new JsonResponseReader(jatisEndpoint.send(new EndpointRequestBuilder()
					.method(HttpMethod.POST)
					.property(HttpHeader.AUTHORIZATION.toString(), "CUSTOM")
					.build())).getContentAsString();
			LOG.info(response);
		} catch (Exception e) {
			LOG.error("Failed to sendSmsJatis : " + e.getMessage(), e);
		}
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
}
