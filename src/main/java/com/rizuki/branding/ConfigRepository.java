package com.rizuki.branding;

import com.rizuki.egov.webutil.persistence.JpaRepository;
import java.io.Serializable;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author Muhammad Rizki
 */
@Dependent
public class ConfigRepository extends JpaRepository<Config, String> implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager entityManager;

	public ConfigRepository() {
		super(Config.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return entityManager;
	}

	public String getConfigValue(String key) {
		Config config = find(key);
		if (config != null) {
			return config.getValue();
		} else {
			return null;
		}
	}

}
