package com.rizuki.branding;

import com.rizuki.egov.webutil.endpoint.EndpointException;
import com.rizuki.egov.webutil.endpoint.EndpointRequest;
import com.rizuki.egov.webutil.endpoint.RestEndpoint;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.client.api.Request;

/**
 *
 * @author Muhammad Rizki
 */
@Dependent
public class JatisEndpoint extends RestEndpoint implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private HttpClient httpClient;
	@Inject
	private ConfigRepository configRepository;

	@PostConstruct
	public void initialize() {
		super.initialize(httpClient, configRepository.getConfigValue(Config.JATIS_URL), null);
	}

	public void initialize(String baseUrl) {
		super.initialize(httpClient, baseUrl, null);
	}

	@Override
	protected Request createHttpRequest(HttpClient client, EndpointRequest endpointRequest) throws EndpointException {
		Request request = super.createHttpRequest(client, endpointRequest);
		return request;
	}

}
