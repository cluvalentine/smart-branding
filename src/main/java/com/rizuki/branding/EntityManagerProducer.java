package com.rizuki.branding;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.persistence.Cache;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@ApplicationScoped
public class EntityManagerProducer{
    @Produces
    @PersistenceContext(unitName = "smart_branding_pu")
    private EntityManager em;
    
    @Produces
    public Cache getCache(){
        return em.getEntityManagerFactory().getCache();
    }
}




