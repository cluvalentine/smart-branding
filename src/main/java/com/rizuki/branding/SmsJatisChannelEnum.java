package com.rizuki.branding;

/**
 *
 * @author Muhammad Rizki
 */
public enum SmsJatisChannelEnum {
	NORMAL_SMS("0"),
	ALERT_SMS("1"),
	OTP_SMS("2");

	private final String value;

	private SmsJatisChannelEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
